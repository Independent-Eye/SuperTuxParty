
# Translations template for Super Tux Party.
# Copyright (C) 2019 Super Tux Party Contributors
# This file is distributed under the same license as the Super Tux Party project.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.7\n"
"PO-Revision-Date: 2019-10-29 23:15:41.029098\n"
"Last-Translator: Automatic generated\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "MINIGAME_NAME"
msgstr "Haunted Dreams"

msgid "MINIGAME_DESCRIPTION"
msgstr "The players must protect the child's bed from ghosts."

msgid "MINIGAME_ACTION_UP"
msgstr "Move Up"

msgid "MINIGAME_ACTION_DOWN"
msgstr "Move Down"

msgid "MINIGAME_ACTION_LEFT"
msgstr "Move Left"

msgid "MINIGAME_ACTION_RIGHT"
msgstr "Move Right"
